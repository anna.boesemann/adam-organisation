\documentclass[a4paper, 11pt]{scrartcl}
\usepackage[english]{babel}
\usepackage[bottom = 2.5cm, top = 2.5cm, left = 3cm, right = 3cm]{geometry}
\usepackage{graphicx}
\usepackage[utf8]{inputenc}
\graphicspath{ {images/}{../images/} }
\usepackage{subfiles}
\usepackage{listings}
\usepackage{pdflscape}
\usepackage{subfigure}
\usepackage{tabularx,ragged2e}
\newcolumntype{C}{>{\Centering\arraybackslash}X} % centered "X" column
\usepackage{hyperref}
\newcommand{\source}[1]{\href{#1}{#1}}
\setlength{\parindent}{0pt}


\begin{document}
\thispagestyle{empty}
\emph{Practical Course on GUI, \\AR and VR development \hfill 3rd May 2018}\\
\rule[5pt]{\columnwidth}{1pt}
\bigskip
\begin{center}
      \textbf{\Huge Requirements Document\\[1ex]
	\Large for ADAM - survey system\\[2ex]}
      \emph{Anna B\"osemann\\Wiebke Dirks\\Maximilian David Eipper\\Lorenz Glißmann\\Jonas H\"ugel}  
  \end{center}
\bigskip\bigskip
\rule[5pt]{\columnwidth}{1pt}
\tableofcontents
\bigskip\bigskip
\rule[5pt]{\columnwidth}{1pt}

\section{Project Introduction}
\subsection{Short description}
\subsubsection*{Basic Scope of the project}
ADAM is an online survey system and should be open source. It provides basic features to create, participate and evaluate surveys. 
The main research focus of the project lies on the evaluation. We aim to provide a method for clustering free test field answers, thus helping researchers to evaluate surveys more quickly and easily.\\
ADAM strives to be of a simplistic and minimal design making it accessible for users with limited computer knowledge. 
It supports the participation in surveys on mobile phones, as well as  tablets, notebooks and desktop PCs, thus reaching out to more participants and providing them with a convenient way to participate in surveys.
The creation and evaluation subsystems are designed for the use on desktop PCs and notebooks. The evaluation report is shown in the browser. Furthermore, ADAM offers an option to export created reports.
ADAM, once finished, should include a responsive Web-UI.\\
Moreover, ADAM tries to include all requirements for survey systems and websites demanded by the General Data Protection Regulation\footnote{\url{https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=celex\%3A32016R0679}} enacted by the European Union.

\subsection{Motivation}
This project descends from student representative work (German: Fachgruppenarbeit, see glossary). As a part of their work the student representatives are trying to improve the quality of the lectures offered by the institute for computer science.
To do so, they need a survey system to acquire the opinions of all the students taking part in those lectures.
The available systems used by the university were not satisfying enough.
Most of them were not usable without charge and inconvenient.
This resulted in the idea to create such a system as an open source software.
Feebback for lectures is often asked for as an open question in the end of a survey in a free text-field. Because it is not feasible to acquire all the problems without free text-field questions and to evaluate those manually, an assisted evaluation is needed.
One of the main problems is that many student reporting the same problems in slightly different ways. In this we found potential for automation and ultimately it resulted in the research question how clustering algorithms can be used to evaluate surveys.\\

After the end of this project (30.09.18) the development process should go on.
Features that were declared optional for this project should be implemented.
Afterwards, among others the software should be used for student representative work.
\subsection{Team}
The team members are Anna B\"osemann, Wiebke Dirks, Maximilian David Eipper, Lorenz Glißmann and Jonas H\"ugel.
Everyone works as an Developer and Tester in the project.
Testing is supervised by Wiebke Dirks and Jonas Hügel, who also serves as main test manager.
Anna Bösemann is the Request Manager. This role is required by the selected Kanban development model. The main task of this role is to prioritize the user stories.
Furthermore, Wiebke Dirks acts as the Kanban Manager. Her task consists of helping to introduce the Kanban methods to the team.
This role was included, because the team never worked with Kanban before.

The team chose a democratic structure for solving problems and discussing the scope of the project.

\subsection{Estimation and Priorisation}
Priorisation is done using the MuSCoW method and the 3-point method. The estimated time needed is 1047 person-hours for all tasks, 719 person-hours excluding user stories that were labeled with 'could'. This results in approximately 10 weeks (15 hours) for each member in order to implement the user stories at hand\footnote{\url{https://docs.google.com/spreadsheets/d/1Rv8uPXNSJ1F5X0jxoiLylqXiWJmlaUwf8hSjW-zR49A/}}. Time spent on writing the final report and preparing presentation is not included in this estimation. Furthermore, additional time will be needed for familiarising ourselves with the used technologies and for setting up and administrating development environments (e.g. git code testing tools, code quality tools...).

\begin{landscape}
\begin{figure}[hb]
    \centering
    \includegraphics[width=0.95\columnwidth]{adamOverview}
    \caption{Overview of the ADAM software.}
    \label{fig:adamOverview}
\end{figure}
\end{landscape}

\section{Personas, User Stories, User Scenarios (minimal scope)}

\subsection{Survey owner}
\subsubsection{Persona Prof. Samuel Eich}
\fbox{\begin{minipage}{\textwidth}
\paragraph{Personal:} Prof. Samuel Eich, 55, 3 children, teaches bioinformatics

\paragraph{Motivations:}
\begin{itemize}
\item gives lecture "Introduction to Machine Learning"
\item wonders why attendance rate is low in his lecture
\end{itemize}
\paragraph{Goals:}
\begin{itemize}
\item gives lecture and wants to assess its quality
\item wants to create his own survey with special questions
\item wants feedback about the quality of his teaching
\item wants to know in which way he can improve his lecture
\end{itemize}
\paragraph{Expectations:}
\begin{itemize}
\item survey works for different devices
\item evaluation should be automated or assisted, because he has no time for manual evaluation
\end{itemize}
\end{minipage}}

\paragraph*{User Stories}
\begin{itemize}
    \item \textbf{EPIC E1: Secure Data Storage}
    \begin{itemize}
        \item As a survey creator, I want a secure system, so that I can access sensitive data as well.
    \end{itemize}
        
    
    \item \textbf{EPIC E3: Survey Creation offers many options}
    \begin{itemize}
        \item As a professor, I want free-text field answers.
        \item As a professor, I want to be able to edit surveys after creating them.
        \item As a professor, I want to understand the UI intuitively, so that I don't have to ask for everything.
        \item As a professor, I want to use dropdown-answer questions.
        \item As a survey creator, I want an undo button.
        \item As a survey creator, I want the system to support multiple language, so that I can address non English-speaking people.
        \item As a survey creator, I want to create individual surveys, so that they fit the needs of the unique case study.
        \item As a survey creator, I want to include pictures into the questionnaire, so that I can ask questions about them.
    \end{itemize}
        
    \item \textbf{EPIC E4: Using Templates for survey creation}
    \begin{itemize}
        \item As a professor, I want to spend at most five minutes per question when creating a survey, so that I can do other work.
        \item As a professor, I want to ask my own questions, although I use the university's question templates.
    \end{itemize}
        
     \item \textbf{EPIC E5: participation}
     \begin{itemize}
        \item As a survey creator, I want the survey to work on mobile devices as well, so that more people fill it out.
        \item As a survey creator I want as many students as possible to finish the survey
    \end{itemize}
        
    \item \textbf{EPIC E6: Create Report for closed survey}
    \begin{itemize}
        \item As a researcher, I want to create reports for closed surveys, so that I can visualize the survey results.
        \item As a researcher, I want to trigger the evaluation of my survey with less than 5 clicks.
        \item As a researcher, I want to trigger the evaluation of my survey multiple times, because I got more answers.
        \item As a researcher, I want to see a boxplot representation of the evaluation of a question
        \item As a researcher, I want to see a piechart for each question in the generated report.
        \item As a researcher, I want to see my generated reports in the web browser.
        \item As a researcher, I want to export my reports.
        \item As a researcher, I want to plot the results of one question in one graph.
    \end{itemize}
        
    \item \textbf{EPIC E7: Free-text-field evaluation}
    \begin{itemize}
        \item As a researcher, I want assistance with processing free-text field answers, so that free-text questions aren't useless for me.
        \item As a survey creator, I want to reuse questions or surveys, so that I can ask certain questions in different surveys and I need less time for creation.
        \item As a researcher, I want to have an option for automated clustering of free-text- field questions to save time.
        \item As a researcher, I want to be able to select the free-text questions fields for that I want clustering.
        \item As a researcher, I want to select the degree of similarity for clustering similar research questions.
        \item As a researcher, I want to select the clustering algorithms, if it is possible to use more than one.
        \item As a researcher, I want that a type x clustering algorithms is used. 
        \item As a researcher, I want to see how many answers got rated as similar by the algorithms.
        \item As a researcher, I want to be able to compare all answers manually that got rated similar to check the correctness.
        \item As a researcher, I want to label free text fields, so that I can use the labels for the report generation.
    \end{itemize}
        
    \item \textbf{EPIC E8: Customizable survey layout}
    \begin{itemize}
        \item As a survey creator, I want individual layout options.
    \end{itemize}
    
    % coulds:
    \item \textbf{extensions:}
    \begin{itemize}
        \item As a researcher, I want the questionnaires to be distinguishable for all participants because I want to do multi-variant statistics on them.
        \item As a survey creator, I want to set start and end time of the survey beforehand.
        \item As a survey creator, I want to use templates so that creating similar surveys cost less time.
    
    \end{itemize}
 
    
\end{itemize}


\paragraph*{User Scenarios}
\begin{itemize}
    \item\textbf{Create survey:}
    Prof. Eich wants to create a new survey. He opens the ADAM website and creates a new  survey. After he added all questions and possible answers he publishes the survey.
    \item \textbf{Evaluate survey:}
    Prof. Eich wants to evaluate one of his surveys. He opens the ADAM website and selects the survey he wants to evaluate. After selecting the evaluation options, he lets the system create a report.  He looks at the report in the web browser, before he exports the report.
\end{itemize}


\newpage


\subsection{Survey participant}
\subsubsection{Persona Eva Heinrich}
\fbox{\begin{minipage}{\textwidth}
\paragraph{Personal:} Eva Heinrich, 18, single, biology, limited EDP knowledge
\paragraph{Motivations:}
\begin{itemize}
\item wants to fill out a survey for her lecture "Introduction to Machine Learning"
\item doesn't like the lecture and doesn't visit it because she thinks it's boring
\item wants to improve the lecture with her feedback
\end{itemize}
\paragraph{Goals:}
\begin{itemize}
\item not to spend much time with survey
\item give good feedback
\end{itemize}
\paragraph{Expectations:} 
\begin{itemize}
\item filling out survey is easy and not complicated
\item good usability
\item looks well on whichever device she uses
\item the professor cannot connect her answers to her face (anonymous)
\end{itemize}
\end{minipage}}

\paragraph*{User Stories}
\begin{itemize}
    \item \textbf{EPIC E2: EU-GDPR implementation }
    \begin{itemize}
        \item As a survey participant, I want to know how my data is stored and used, so that I can decide how I answer in the survey.
        \item As a student, I want to fill out questionnaires anonymously.
    \end{itemize}
    \item \textbf{EPIC E5: participation}
    \begin{itemize}
        \item As survey participant, I want to be able to fill out surveys independently from the device I am using, so that I don't to use special hardware or software.
        \item As a survey participant, I want to use my phone.
        \item As a survey participant, I want to use my desktop PC to fill out the survey.
        \item As a survey participant, I want to know how far into the survey I am
    \end{itemize}
    
    \item \textbf{extensions:}
    \begin{itemize}
        \item As a survey participant, I want to use my keyboard for navigation, so that I can spend less time on each survey page.
        \item As a student, I want to be able to change the language when filling out a questionnaire.
    \end{itemize}
\end{itemize}


\paragraph*{User Scenarios}

\begin{itemize}
    \item \textbf{participate at survey:} 
    Eva Heinrich wants to participate in a survey. She opens the survey in the web browser. Before filling out the survey, she accepts the data protection policy. After she answered all the questions, that she wants to answer, she submits the survey.
\end{itemize}
\newpage

\subsection{Data Protection Commissioner (DPC)}

\subsubsection{Persona: Edward Schnee}
\fbox{\begin{minipage}{\textwidth}
\paragraph{Personal:} Edward Schnee, 37, married, works as Data Protection Commissioner for the university

\paragraph{Motivation:}
\begin{itemize}
    \item part of his job
    \item interested in Data Protection
    \item not get his university sued
\end{itemize}
\paragraph{Goal:}
\begin{itemize}
\item implement General Data Protection Regulation (GDPR)
\item protect users
\end{itemize}
\paragraph{Expectations:}
\begin{itemize}
\item the system itself is GDPR compliant
\item the system makes it easier for him to do the tasks he should do as DPC
\end{itemize}
\end{minipage}}

\paragraph*{User Stories}
\begin{itemize}
    \item \textbf{EPIC E2: EU-GDPR implementation }
    \begin{itemize}
    \item As a DPC, I want that everyone has to actively click a button to accept the data protection notice before doing anything.
    \item As a DPC, I want that the system stores no personal information until the user agrees to that because of the GDPR. 
    \item As a DPC, I want the system to encrypt all stored personal data because of the GDPR.
    \item As a DPC, I want the system to encrypt all data transfers to the user with SSL because of the GDPR.
    \item As a DPC, I need a GDPR conform system description.
    \item As a DPC, I want to export all personal information about someone because anyone can request their data (based on the GDPR).
    \item As a DPC, I want to delete all information stored about someone.
    \end{itemize}
    
    \item \textbf{extensions:}
    \begin{itemize}
        \item As a DPC, I want the system to automatically remove all personal information once it isn't needed any more because of the GDPR.
    \end{itemize}
    
\end{itemize}

\paragraph*{User Scenarios}

\begin{itemize}
    \item \textbf{Validate survey:}
    Edward Schnee receives a request to validate a survey. He opens the survey in a browser and checks the data protection policy. Afterwards, he checks if data that is acquired in the survey matches the data described in the data protection policy. In the end he checks if the survey used a encrypted connection to the server. He then either accepts or declines the survey.
    \item \textbf{Export user data:} 
    Edward Schnee receives a survey participant request. The user wants to see the data stored about him. Edward Schnee  selects the correct surveys, exports the data and sends it to the user.
    \item \textbf{Delete user data:}
     Edward Schnee receives a survey participant request. The user wants the data stored about him deleted. Edward Schnee selects the correct surveys and deletes the data about the user.
\end{itemize}
\newpage



\newpage
 \section{Sketches and Wireframes}
%\subfile{wireframesDescription}

\section{Further ideas}
The following personas and described features could be of interest to have a well-working system not just for people who are experienced with the system or those who got an introduction to the system. An example is a more user friendly interface for creating surveys. It is not necessary for our purposes since our research question focuses on the evaluation of survey answers and pre-processing the free text-field answers.\\
\\
The personas and user stories in this chapter are not complete and would have to be refined and reworked before being implemented. The last subsection provides a list of unspecified ideas. For now the focus is on the basic scope.


\subsection{Survey owner}
\subsubsection{Persona Isabel Petterson}
\fbox{\begin{minipage}{\textwidth}
\paragraph{Personal:} Prof. Isabel Petterson, 42, married, 1 child, professor for psychology, Language skills: swedish: natural, english: fluent, german: good, works at Uni Göttingen
\paragraph{Motivations:}
\begin{itemize}
\item wants to use surveys for psychological study in a research project, includes many free text fields
\end{itemize}
\paragraph{Goals:}
\begin{itemize}
\item export raw data from time to time to apply continuous evaluation
\item give partial access (survey administration) to research assistant, due to data protection policies
\end{itemize}
\paragraph{Expectations:}
\begin{itemize}
\item patient-data should be stored securely
\item provide survey in different languages
\item setup survey using an interface in English language
\item intelligent assistance for free text field evaluation
\end{itemize}
\end{minipage}}

\paragraph*{User Stories}
\begin{itemize}
    \item \textbf{EPIC E1: Secure Data Storage}
    \begin{itemize}
        \item As a researcher, I want a secure system, so that I can store sensitive data.
    \end{itemize}
    
    \item \textbf{EPIC E3: Survey Creation offers many options}
    \begin{itemize}
        \item As a researcher, I want to include many free text field into the survey, so that I can enquire about pernoal attitudes.
        \item As a researcher, I want to include pictures into the questionnaire, so that I can ask questions about them.
        \item As a researcher, I want the system to work for 500-2000 survey participants
        \item As a researcher, I don't want a multi steps user interface, so that the program responds quickly.
    \end{itemize}
    
    \item \textbf{EPIC E4: Create Report for closed survey}
    \begin{itemize}
        \item As a researcher, I want the system to load each view in less then 1 second,  even if there are multiple persons working on it.
        \item As a researcher, I want to be able to export raw data to do sophisticated manual analysis on them.
    \end{itemize}
    
    \item \textbf{EPIC E8: Customizable survey layout}
    \begin{itemize}
        \item As a researcher, I want individual layout options.
    \end{itemize}
    
    \item \textbf{extensions:}
    \begin{itemize}
        \item As a researcher, I want to be able to give partial access to my research assistants, so that I can fulfill data protection policies.
        \item As a researcher, I want the system to have an integrated scanning function, so that I can use on paper questionnaires
        \item As a researcher, I want to be able to print the questionnaire, so that I can use it in schools that don't have internet or computers.
        \item As a researcher, I want to be able to pin my questionnaires together, so that I don't get confused with longer questionnaires.
        \item As a researcher, I want verifying to be on a single page, so that I don't have to click through every single question.
        \item As a researcher, I want to be able to create different user profils and limit right for those, so that I can delegate work to my student assistants.
        \item As a researcher, I want the system to be inexpensive because my department doesn't have a lot of money
        \item As a researcher, I want to be able to use a layout similar to the IGLU and TIMMS studies.
    \end{itemize}
\end{itemize}

\paragraph*{User scenario}
\begin{itemize}    
    \item \textbf{Raw data export: }Prof. Petterson wants to export the raw data. Therefor she opens the ADAM website and selects the survey and checks that the manual verification is completed. Afterwards, she exports the data as csv, archives the survey and downloads the archived survey.
    \item \textbf{create account with restricted access rights} Prof. Petterson wants to create an account for one of her assistance. She opens the ADAM website, logs in and creates a new user profile (e.g. for a student assistant). Then she selects the rights for the user profile (only allowed to do verification) and logs out.
\end{itemize}
\newpage


\subsection{University Administration}

\subsubsection{Persona Andrea Ringbiel}    
\fbox{\begin{minipage}{\textwidth}
\paragraph{Personal:} Mrs. Andrea Ringbiel, 43, married, works for the university in  the quality assurance (QA) department and  uses a normal university windows setup
\paragraph{Motivations:}
\begin{itemize}
\item generally isn't very motivated and therefore wants to spend as little time as possible
\item part of her job
\end{itemize}
\paragraph{Goals:}
\begin{itemize}
\item create reports for all faculties 
\item wants to create templates for all faculties (lecturers)
\end{itemize}
\paragraph{Expectations:}
\begin{itemize}
\item expects good work flow (fast usage)
\item not having to add users by hand
\end{itemize}
\end{minipage}}

\paragraph*{User stories}

\begin{itemize}
    
    \item \textbf{EPIC E3: Survey Creation offers many options}
    \begin{itemize}
        \item As a QA, I want to invite the participants directly via E-Mail from my user interface to save time.
        \item As a QA, I want to share the result of the survey via mail with other.
    \end{itemize}

    \item \textbf{EPIC E8: Create Report for closed survey}
    \begin{itemize}
        \item As an organizational administrator, I want an easy to use  system, because I don't like learning new things.
        \item As an organizational administrator, I want the data export to use corresponding newlines under Windows, Linux and MacOS.
        \item As an organizational administrator, I want to use windows, so that I can do it on my work computer.
    \end{itemize}
    
     \item \textbf{extensions:}
     \begin{itemize}
         \item As an organizational administrator, I don't want to be forced to add user by hand, so that I can deal effectivly with large student groups.
         \item As a QA, I want to select multiple of my surveys in the overview and trigger their evaluation with one click to work more efficient.
         \item As a QA, I want to be able to choose from activating a back-button or deactivating the button.
     \end{itemize}
\end{itemize}

\paragraph*{User scenario}
\paragraph{Create survey template}
\begin{itemize}
    \item \textbf{Create survey template:}
    Andrea Ringbiel wants to create a survey template that should be equal for all departments. She creates a template with these questions and marks them as non editable. Then she shares the template with all faculties.
    \item \textbf{Create report over all surveys}
    Andrea Ringbiel has to write a report about all (university wide) surveys. So she opens her template and exports all the results for derived surveys.
\end{itemize}
\newpage

\subsection{Technical Administration}

\subsubsection{Persona Rolf Sebastian}
\fbox{\begin{minipage}{\textwidth}
\paragraph{Personal:} Mr. Rolf Sebastian 27, married, works for technical department of the university, manages many university servers
\paragraph{Motivations:}
\begin{itemize}
\item part of his job
\end{itemize}
\paragraph{Goal:}
\begin{itemize}
\item wants to set up studIP
integration
\item wants to set up the survey system for all users of the university
\item wants to integrate the system with LDAP/Active Directory (user 
\end{itemize}
\paragraph{Expectations:}
\begin{itemize}
\item he cannot see survey data
\item not having to add users by hand
\item low maintenance
\end{itemize}
\end{minipage}}

\paragraph*{User Stories}
\begin{itemize}
    \item \textbf{EPIC E1: Data Storage}
    \begin{itemize}
        \item As a technical administrator, I want the data to be hidden, so that the privacy of survey participants is ensured.
    \end{itemize}
    
    \item \textbf{extensions:}
     \begin{itemize}
        \item As a technical administrator, I want to set up the survey system for all users of the university, so that I can address everyone at the same time.
    \end{itemize}
    
\end{itemize}

\paragraph*{User Scenarios}
\begin{itemize}
    \item \textbf{Set up the system}
    Rolf Sebastian has to setup the survey system. He installs the server with an encrypted database and configures the LDAP-Login and the StudIP-plugin.
\end{itemize}
\newpage

\subsection{reserach question related user stories}
\begin{itemize}
        \item There are not many user stories concerning this topic as the personas are not interested in how we achieve that the participants actually finish the survey. Only the participants themselves want some kind of feedback, which we will provide by using a progress bar
        \item As a survey creator I want as many students as possible to finish the survey
        \item As a survey participant, I want to know how far into the survey I am
\end{itemize}
\newpage

\subsection{List of ideas without further context}
\begin{itemize} 
    \item system features that specialize the software for university purposes, including lecture evaluation and surveys for research purposes
    \item assisted reviewing during evaluation (e.g. manual clustering)
    \item interactive surveys (e.g. showing a question only if previous was answered in a defined way)
    \item intelligent progress bar
    \item provides automatic sophisticated statistical-evaluation
    \item assisted report generation for fullfilling GDPR
    \item sophisticated university-specific user-management (e.g. login for dean of studies with views for their specific tasks)
    \item share surveys with other and limit their access to the survey (e.g. only allow access on a part of the system)
    \item large range of different question types
    \item support for providing one survey in different languages
    \item sophisticated assisted free-text field evaluation for free-text answers with more than 30 words
    \item integration with uniVZ, so that lecture details could be provided automatically
    \item integration with studIP, so that evaluation survey can be included directly in the corresponding lecture in studIP
    \item providing printable version for surveys
    \item scanning feature for printed surveys, including assisted recognition of chosen answers


\end{itemize}


\end{document}
