# Planung

## Zeitplanung
* June, 9th (Saturday)
* 10 -  10:30 Uhr Stand-Up
* 10:30 -12 Uhr IDE aufsetzen
* 12 Uhr Mittagspause
* ab ca 14 Uhr Javascript lernen (versuchen testdriven)
* ca 17 Uhr Tagesrückblick und Planung für den Sonntag
* Gleitzeit-Ende: ab 18 Uhr
* June, 10th (Sunday)
* 10 -  10:30 Uhr Stand-Up
* 12:30 - 14 Uhr "angeordnete" Mittagspause
* 16:30 - 17 Uhr Stand-Up
* Gleitzeit-Ende: 18-20 Uhr

## Ideen hinter der Planung
* Gleitzeitregelung 
* ist üblich in der Arbeitswelt
* bildet m.M. nach unsere individuellen Arbeits-/Schlaf-/Freizeitzeiten besser ab, als eine fixe Zeit
* @Wiebke: Zugang ab 8 Uhr für dich zum gewählten Raum, wird kein Problem sein
* ein fester Raum  (0.101 oder 1.101) 
* Samstag Mittagessen im Kaiserpalast (Mittagsbuffet): 
ASIATISCHES MITTAGSBUFFET 
Mo. bis Sa. von 11:30 - 14:30 Uhr (außer an Sonn– und Feiertagen)
pro Person 7,50 €
* Samstag Abendbrot
* Sonntag Mittagessen: etwas bestellen, vllt Indisch?

## Ideen für Themen/Tasks
* Javascript Grundlagen gemeinsam erarbeiten, weil das von Ole kein Guter Einstieg war (Lorenz meldet sich freiwillig)
* Arbeit teilweise in Zweiergruppen
* Talk about Tutorial userstories/wireframes/etc. 
* Refactor Glossary
* Talk about research question? (Decide on what to tell Patrick)



