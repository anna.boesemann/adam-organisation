Patrick's comments:
    
    * didnt say much
    * commented that we did a mix of delphi and three point
    * upon asking I showd the user story file and the adam board. I went through some user stories
      and explained them. also explained how ours board works
    
    
fossils:
* fossils groud also uses a kanban like board in github
* have milestones and big issues, but not details tasks
* they dont think they will be time pressured
* already included report writing
* did three point method
* drew wireframes
* did not do estimations based on user stories

VR presentation group:
    * wireframes, estimation
    * function requirements
    * showed wireframes
    * explained in detail how system should look like
    * estimated 300 hours work altogether
    * plan to use many templates
    
key management system
* little bit behind bc change of topic
* did swag estimation
* unprecise estimation (weeks not hours)
* did cost analysis
* PHP framework

patrick:
    * programmers are paid 120 Euros per hour, minimum 100 euros per hour
    * not happy ("my funniest day, your worst day)
    * group estimation how much work we put in: pppw 6-15 hours per week
    *general feedback
* unhappy that people sent in backlogs late (friday or sunday) ... meint er uns auch mit Freitag?
  warhscheinlich nicht. wie gesagt, general feedback, he doesnt specify groups
* if late, then send a note to ask for extension
* issues with sketches: not all user stories covered
* do: description: with this wireframe we want to cover... user story /scenario, this wireframe is
  for persona xyz
* he commented on wireframes in his feedback. i havent seen his feedback. so cannot say wheather it
  applies to us or not
* doesnt like statements like "the system will be easy to use"
* resource estimation
* way too optimistic
* include customer contacs
* never say you are unsure with estimation
* project states
* says all groups are behind
* one group criticises patricks course structure: work for monday
* we work just to fulfil his tasks, we should do more
* work on research question
* groups should devide user stories in must, could...
* plan case studies for research question
* one group complains about too much overhead
* patrick quite devensive
* all wireframes seen so far are horrible
* our wireframes were the best!!!
* now uses them as an example to explain what can be done better
* no flshy colours on right side of website
* what will be hidden behind menu (on website)
* demands detailed description
* what are the icons about?
* our wireframes would fail usability engeneering tests why? he will say later, he wants to
  introduce usability e today
*  

tasks:
    * condider user stories, draw wireframes
    * check that we have wireframes for most important user stories
    * start expert oriented usability engeneering
    * DO: cognitive walk through
* need expert an requirements
* take user scenario and wireframes, ask four questions, if no: issue detected
* prevent designing own icons
* he recommends to add text
* consider a way to teach user icons for new icons
* on first app start: highligts and explains functinoality

# Planung für Woche 
## Unsorted 
0. Interview DPC - Vorbereitung  (Deadline 14/05 um Montag beim Treffen kurz vorstellen) 
1. More wireframes from user stories im basic scope
* check that we have wireframes for most important user stories
2. Draw more wireframes
3.  Überarbeitung der Interfaces -write missing userstories
3.1 Existing kanban card
3.2 new: include user stories in table, user story document, requirements document
3.3 estimation of new stories in table
4. Sub-Document for Wireframes (creating)
* Create in RequirementsDocument project in share-latex 
* include wireframes (IDs with W<x> wobei x eine Zahl)
* subsection "description" for each wireframe
* subsection  "relating user stories" for each wireframe
5. Sub-Document for Wireframes (new content)
* fill subsection with content for each existing wireframe
* include new wireframes
6. plan cognitive walkthroughs
* inform yourself about and create wiki entry with short overview 
* think of possible walkthroughs for "participation" subsystem
* for each thought-of-WT make list of missing things 

Wireframes creation:

- weiße felder auf dem homeview entfernen oder mit "enter name" beschriften (entfernen erstart mehr
  arbeit)
- wireframes direkt nach dem homeview: auf dem man den namen der survey eingeben kann, über
  Datenschutzgrundverordnung belehrt wird und evtl ein tutorial von adam bekommt
- im moment kann man sowohl links in der structure bar als auch ganz recht eine neue
  frage/evaluation creieren. da sollten wir ein rausschmeißen, sonst müssen wir für alles immer
zwei wireframes zeichnen.


Wireframes für participation:


- example participation storyboard for mobile devices am ende den goodbye screen einfügen
- ansonsten würde ich das tatsächlich so lassen, walkthrough für alle participation scenarios
  bestanden.


7. conduct cognitive walkthroughs
* possibly not in scope until Monday (would be possible on Tuesday or Wednesday after KIF) ...
  we'll see 
8. extend requirements document 
* by estimations (nur Akkumulierte Informationen und Link zur table) 

## Dependencies
* 2 depends from 1
* 5 depends from 4
* 7 depends from 6 and 2

## Plan/Aufgaben (in Telegram)
* 



final presentation (end of project): mid september
