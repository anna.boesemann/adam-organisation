# Treffen 2018/04/19
Link zum Pad: https://pad.informatik.uni-goettingen.de/p/2018-04-19-PP

## Ziele:
1. Projektorganisationsmodell festlegen
2. Rollenverteilung 
3. Weekly Task: Requirements Engeneering
4. Überarbeitete und erweiterte Personas+
5. product scope festlegen

## Tagesordnung und Protokoll (10min)
1. Bericht vom Interview mit der Evaluationsabteilung von Studium & Lehre (10min)
2. Projektorganisation (15min)
3. Rollenverteilung (30min)
4. product scope (45min)
5. Requirements Engeneering (45min)


## Bericht vom Interview mit der Evaluationsabteilung (10min)
* viele Anforderungen genannt:
    * teilweise existierende Features: filterführung, mehrsprachigkeit
    * teilweise wünsche: templates, browser-kompatibilität, (quest back mmacht da probleme) opt-out (möglichkeit haben, fragebogen gar nicht erst zu beginnen)
* life Auswertungsstatistiken (wer hat bis wohin bearbeitet?)
* einige Stellen, wo noch recht viel manuell gearbeitet wird
* intuitive Nutzung gerade nicht möglich, stellt einige Probleme dar
* Es gibt Rechtverwaltung, aber es haben Leute zugriff auf Rohdaten
* Personen ansprechen: Herr Hallaschke als Datenschutzbeauftrager
* Personen ansprechen: Herr Winkler für univz Anbindung
* mehrere Dokumente bekommen, die Aufschlüsse über must-haves geben können: verschiedene Datenschutzrichtlinien
* für uns hat sich noch ergeben, bei den Digital Humanities bzgl. des CLustering anzufragen
* Waren sehr offen. Ein äußerst Produktives und angenehmes Gespräch.
* Frau Klingenbiel bzg studip anbindung kontaktieren 

## Projektorganisation (45min)
### Was steht zur Auswahl und DIskussion? 
* Diskussion: kanban, scrum, xp
* scrum starre rollenverteilung
* kanban board nur digital möglich
* xp flexibel, aber vtl ein bisschen zu flexibel
* entscheidung? iterativ oder cyklen? Iterativ! passt besser zum unialltag --> kanban!

###Was wollen wir bentuzen? kernlelemente
* product backlog
* kanban board
* spezifikationen für kanban board
* wie priorisieren wir im kanban board? 
* stand-up meeting
* user stories auf kanban cards schreiben
* praktiken von XP: 
    * collective code ownership
    * Continuous integration
    * test-driven
    * simple design
    * sustainable pace (reflexion geschieht in stand up meetings)
    * pair programming
* brauchen was für research part

### Dokumentation:
* haben product backlog
* orga ins gitlab
* report preparation
* wöchentliche aufgaben


## Rollenverteilung (30min)
### Stand, was wir mögen:
* Anna: Interface Design, CSS, Fragestellung: Clustering, Teamarbeit, nicht Testen 
* David: usaibility, backend, Programmieren, Clustering-Praktisch, nicht Front-End-Style
* Lorenz: Backend, wissenschaftliche Fragestellungen, nicht Testen
* Jonas: Testen eines großen Projekts, Programmierung, Planung, kein PHP
* Wiebke: Testing, Front-End, nicht Mathe, nicht wissenschaftliche Fragen, Report

### Roles
* Service Delivery manager (flow manager)
    * checkt kanban board
    * "watchdog"
    * -> Wiebke
* Service Request Manager
    * manages product backlog
    * -> Anna 
* Team roles:
     * Anna: Designer, scientific supervisor
     * David: engineer, scientific supervisor
     * Lorenz: architect, engineer
     * Jonas: Test manager, architect
     * Wiebke: Designer; Tester
     * all: programmer

## product scope festlegen (45min)
### Minimal frame und mindestens 1 Forschungsfrage
* create questionnaire
    * question types: single choice, multiple choice, free text field
    * without UI
* fill out questionnaire
    * with responsive web UI
* clustering of free text questions -> scientific question
* scalability (ca. 300 participants)
* export of data
* integration of Datenschutzgrundverordnung (new from May 25th)
* Forschungsfrage:  To which extend can (insert algorithm) clustering help to evalutate free text field asnwers in surveys

### Ergänzung für unser Projekt und weitere Forschungsfragen
* web interface for creating questionaires (prefered for desktops)
* automated evaluation
* basic user management

### Nice To Haves und weitere Forschungsfragen
* interactive questionnaires
* report generation (e.g. plots, graphs)
* intelligent   progress bar
* studIP-integration
* uniVZ-integration
* advanced user management
* questionnaire templates
* scientific question: automatic realization of anonymization (e.g. add noise)

### Abschließend: 5 Satz Beschreibung unseres Produktes mit Ergänzung durch geplante Forschungsfragen
LORENZ

## Requirements Engeneering (30min)
### Grafik von Montag sichten: Was steht an? 
* elicitavion
    * done: personas, user stories, interviews, i
    * to do: interview analysis, conduct interviews, make an appointment with data security manager of Göttingen University (Hallaschka), work through received documents -> more user stories
* interpretation:
    * done:
    * to do: nterview analysis, structure user stories, identify requirements from known user stories
* negotiation
    * done: define product scope
    * to do: ignore nice to have for now, prioritise user stories, 
* documentation
    * done: latex document with user stories
    * to do: product backlog
* validation:
    * to do: check if requirements fulfil product scope, formal verification

### Aufgaben definieren und verteilen 
* jonas: latex überarbeiten
* wiebke: user stories (inhaltlich)
* lorenz und wiebke: interview, nachbesprechung 11 Uhr montag
* david, anna, jonas: interview analysieren inprogress
* anna: prepare presentation ok
* anna: kanban board einrichten ok
* Montag: talk about presentation, 5sentence-scope, Stubbe Kurzbericht
* Jonas: Kontakt mit Stellv. Datenschutzbeauftragten der Uni
* Anna und WIebke: lernen was rollen genau machen
* Lorenz, Jonas: task tracker identifizieren, Jira anschreiben, freeware suchen, Ergebnis Montag ok
* 5 Satz Beschreibung unseres Produktes mit Ergänzung durch geplante Forschungsfragen (Lorenz)
* Ole macht js Einführung am 3.5.18. (Lorenz)
