# Trefen 26/04/2018

Link zum Pad: https://pad.informatik.uni-goettingen.de/p/2018-04-26-PP

Beginn:
Moderation: Jonas
Protokoll: David

##Ziele:
* Zeitschätzung abgeschlossen
* Priorisierung abgeschlossen

##TO
* TO1: Time Estimation (60 min)
* 10 min Pause
* T02:  Prioritiisation (60 min)
* 10 Min Pause
* TO3: Übertragung auf FunctionPoint Methode (falls Zeit )

##TO1: Time Estimation

##T02:  Prioritisation(60 min)
* jeder übernimmt einen Part
* Abhängigkeiten in User Stories erkennen 
* Constraints, nicht funktionale schwer testbar

##TO3: Function Point

## Tasks
* Rausfinden, was ISO9001 ist/macht
