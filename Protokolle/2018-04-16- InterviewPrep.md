# Vorbereitung Interview mit Frau Klingebiel
Link zu Pad: https://pad.informatik.uni-goettingen.de/p/2018-04-16-PP-InterviewVorbereitung

Interview-Protokoll: https://pad.informatik.uni-goettingen.de/p/2018-04-18-PP-InterviewKlingebiel

Location: Wilhelmsplatz 2

Time: 9:30 bis 10:30 Uhr, 9:15 Uhr Treffen am Wilhelmsplatz

Stakeholder: Studium und Lehre, Anke Klingebiel, Qualitätssicherungsinstrumente

## Goals from Stakeholders view?
* Product vision
*Budget
* Schedule
* Technical constraints
* Business goals
*Customers and users
* Learn about competitors
* What they are trying to achieve
* Success criteria



## Interview
### Goals
* Anforderungen (functional, non-functional and constraints) der Uni an ein Umfragensystem zur Veranstaltungsevaluation
* Anforderungen anderer Nutzer an ein Umfragesystem auch für erweiterte Zwecke
* Nutzergruppen ermitteln
* Datenschutzrichtlinien der Uni (intern und extern) bzgl. Umfragen/Datenerhebung

### Roles
* Talking with guideline: Jonas
* Note taking, comments: Anna
* Protocol: David


### Schedule of Interview
Introduction: 
    * Team
    * Our objectives
    

### Interview questions
1 - Anforderungen: 

    * Welche Anforderungen stellt die Uni an ein Onlineumfragesystem? !!!!!!!!!!!!!!!!! OK
    * Welche Funktionalitäten muss ein Onlineumfragesystem erfüllen? 
    * Welche Herausforderungen sehen Sie bei einem Onlineumfragesystem, dass an Universitäten genutzt wird? (Warum?) OK
    * An welchen Stellen muss bei der Veranstaltungsevaluation besonders viel manuell gearbeitet werden? OK
    * Welche Zugriffsbeschränkungen unterschiedlicher Rollen sind in Evasys implementiert?
    * Gibt es weitere Zugriffskontrollmechanismen, die ihrer Meinung nach notwendig wären? 
    * Sind in EvaSys für alle Rollen die Daten vollständig anonymisiert? OK
    * Welche Funktionalitäten sind auf welcher Art von Endgeräten unbedingt erforderlich? OK
    
2 - Nutzergruppen:

    * Welche Personengruppen nutzen EvaSys?
    * Gibt es auffällige Häufungen von Beschwerden bestimmter Gruppen zu EvaSys?
    * Welches ist die Hauptnutzergruppe? 
    
3 - Datenschutz:

    * Welche Datenschutzrichtlinien muss die Uni nach außen erfüllen? OK
    * Gibt es interne Datenschutzrichtlinien, die sich die Uni selber auferlegt? OK
    
4 - Diverses:

    * Welche Richtlinien gibt es bezüglich Einbindung von StudIP-Plugins? -> haben "Fragebogen" gesehen, ist es Feature oder Plugin von StudIP OK
    * Gibt es weitere Personen, von denen Sie denken, dass wir mit diesen sprechen sollten? Wenn ja, wer? OK
    * Inwiefern 
    


* What should this project accomplish for the business?
* How will you, personally, define success for this project?
* Is there anyone you think we need to speak with who isn’t on our list? Who?
* How would you like to be involved in the rest of the project, and what’s the best way to reach you?


### Things to watch out for:
* Presumed constraints—ask why they are constraints
* Jumping to solutions—ask what problem the solution would solve