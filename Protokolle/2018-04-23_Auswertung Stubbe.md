https://pad.informatik.uni-goettingen.de/p/userstoriesstubbe

Stubbe Interview:

Persona: Professor Stubs
* 50, 2 Kinder
* Professur in der quantiativen Bildungsforschung

User Stories:
* As professor Stubs, I want individual layout options.
* As professor Stubs, I want to include pictures into the questionnaire, so that I can ask questions about them.
* As professor Stubs, I want to be able to print the questionnaire, so that I can use it in schools that don't have internet or computers.
* As professor Stubs, I want the system to work for 500-2000 survey participants
* As professor Stube, I want to be able to pin my questionnaires togesther, so that I don't get confused with longer questionnaires.
* As professor Stubs, I don't want a multi steps user interface, so that the programm responds quickly
* As professor Stubs, I want the questionnaires to be distinguishable for all participants because I want to do multivariant statistics on them
* As professor Stubs, I want the system to be fast, even if there are multiple persons working on it
* As professor Stubs, I want the system to have an integrated scanning function, so that I can use on paper questionnaires
* As professor Stubs, I want verifying to be on a single page, so that I don't have to click through every single question.
* As professor Stubs, I want the system to be inexpensive because my department doesn't have a lot of money
* As professor Stube, I want to be able to use a layout similar to the IGLU and TIMMS studies.
* As professor Stubs, I want to be able to export raw data to do sophisticated manual analysis on them
* As profesor Stube, I want to be able to create different user profils and limit right for those, so that I can delegate work to my student assistants.



Anforderungen:
    * eindeutig zuornenbare Fragebögen (id-System) für Längsschnittstudien
    * Datenexport
    * Formatierung flexibel
    * viele Formatierungsmöglichkeiten
    * Möglichkeit Bilder einzubinden
    * muss offline funktionieren
    * on paper
    * scanfunktion
    * verifizieren auf einer seite
    * lösung des tackerproblems
    * mehrseitige fragebögen müssen getackert werden können ohne das scanfunktion beeinträchtigt wird
    * schnelles system, auch wenn mehrere personen zugleich dran arbeiten
    * Studiengröße 500-2000 Teilnehmer
    * Layout an iglus und TIMMS orientieren
    * ids sollen beliebig vergeben werden können
    * kein mehrstufiges user interface
    * open source wäre schön oder kostengünstig (evasys kosten uni 7-stellige summe)
    
    
    #user scenarios
    
    student assistant
    
    * scan questionnaire
    * log in
    * upload scan to system
    * start automatic answer detection
    * manual verification
    * log out
    
   #  professor Stubs
   ## scenario 1
    
    * log in
    * select survey
    * check that mannual verification is completed
    * export data as csv
    * archive survey and download archived survey
    * log out

## scenario 2

 * log in
 * create new user profile (e.g. for student assistant)
 * select rights for user profile (only allowed to do verification)
 * log out

